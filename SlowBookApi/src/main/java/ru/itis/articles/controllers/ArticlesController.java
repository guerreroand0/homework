package ru.itis.articles.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.articles.entities.Article;

import java.util.List;

@RestController
@RequestMapping("/articles")
public class ArticlesController {

    @GetMapping()
    public List<Article> getAllArticles() throws InterruptedException {
        Thread.sleep(5000);
        return List.of(
                Article.builder()
                        .title("Война и мир")
                        .author("Толстой Л.Н.")
                        .description("Эпический роман, опубликованный в 1869 году, повествующий о жизни российского общества во времена наполеоновских войн")
                        .build(),
                Article.builder()
                        .title("Преступление и наказание")
                        .author("Достоевский Ф.М.")
                        .description("Роман, опубликованный в 1866 году, рассказывающий о преступлении бедного студента и его нравственных муках")
                        .build(),
                Article.builder()
                        .title("Мастер и Маргарита")
                        .author("Булгаков М.А.")
                        .description("Роман, написанный в 1928-1940 годах, который рассказывает историю визита сатаны в атеистический Советский Союз")
                        .build(),
                Article.builder()
                        .title("Анна Каренина")
                        .author("Толстой Л.Н.")
                        .description("Роман, опубликованный в 1877 году, повествующий о трагической судьбе женщины, нарушившей общественные устои")
                        .build(),
                Article.builder()
                        .title("Идиот")
                        .author("Достоевский Ф.М.")
                        .description("Роман, впервые опубликованный в 1869 году, повествующий о судьбе князя Мышкина, который возвращается в Россию после лечения в Швейцарии")
                        .build()

        );
    }
}
